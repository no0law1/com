function [ key ] = decode_dtmf( x )
    Fs = 8000;
    mapObj = createDTMFMap();
    
    allKeys = keys(mapObj);
    
    t = 0 : 1/Fs : 1;
    
    for i=1 : length(allKeys)
        value = cell2mat(values(mapObj, allKeys(i)));
        f1.xi = sin(2*pi*value(1)*t);
        f2.xi = sin(2*pi*value(2)*t);

        if x == signal_sum(t, f1, f2)
            key = allKeys(i);
            break;
        end
    end
end

