function [ decodedSequence ] = RepetitionDecoding( encodedSequence )
    % Repetition code (3,1)
    encodedIndex = 1;
    decodedSequence = zeros(1, length(encodedSequence)/3);
    
    for i=1 : length(decodedSequence)
        bit = encodedSequence(encodedIndex : encodedIndex+2);
        if isequal(bit, [0,0,1]) || isequal(bit, [0,1,0]) || isequal(bit, [1,0,0]) || isequal(bit, [0,0,0])
            decodedSequence(i) = 0;
        end
        if isequal(bit, [1,0,1]) || isequal(bit, [0,1,1]) || isequal(bit, [1,1,0]) || isequal(bit, [1,1,1])
            decodedSequence(i) = 1;
        end
        encodedIndex = encodedIndex + 3;
    end
    
end

