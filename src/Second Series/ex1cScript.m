bits = file2bit('a.txt');
allBits = [bits, bits, bits, bits];

% Bits all corect
[x, t] = BandEmitter(allBits, 1, 100, 1, 'PSK', 10, pi/2);
plot(t,x);
y = Canal(x, 0.8, 20);
result = BandReceiver(y, t, 1, 1, 100, 10, pi/2, 'PSK');
disp(' Attenuation 0.8, SNR 20');
disp(isequal(result, allBits));
disp(' Bit Error Rate');
disp(BER_Calculator(allBits, result));

% Not all bits correct due to attenuation
[x, t] = BandEmitter(allBits, 1, 100, 1, 'PSK', 10, pi/2);
plot(t,x);
y = Canal(x, -0.5, 20);
result = BandReceiver(y, t, 1, 1, 100, 10, pi/2, 'PSK');
disp(' Attenuation -0.5, SNR 20');
disp(isequal(allBits, result));
disp(' Bit Error Rate');
disp(BER_Calculator(allBits, result));

[x, t] = BandEmitter(allBits, 1, 100, 1, 'PSK', 10, pi/2);
plot(t,x);
y = Canal(x, 0.5, 20);
result = BandReceiver(y, t, 1, 1, 100, 10, pi/2, 'PSK');
disp(' Attenuation 0.5, SNR 20');
disp(isequal(allBits, result));
disp(' Bit Error Rate');
disp(BER_Calculator(allBits, result));