% Without Error Correction
allBits = file2bit('a.txt');

[x, t] = BandEmitter(allBits, 1, 100, 1, 'OOK', 10, 0);
plot(t,x);
y = Canal(x, 0.6, 24);
result = BandReceiver(y, t, 1, 1, 100, 10, 0, 'OOK');
bit2file(result, 'a1WithoutCorrection.txt');

% With Error Correction
allBits = RepetitionEncoding(allBits);
[x, t] = BandEmitter(allBits, 1, 100, 1, 'OOK', 10, 0);
plot(t,x);
y = Canal(x, 0.6, 24);
result = BandReceiver(y, t, 1, 1, 100, 10, 0, 'OOK');
result = RepetitionDecoding(result);
bit2file(result, 'a1WithCorrection.txt');