%
% Function Works only with codes 'OOK' -> Unipolar and 'PSK' -> Bipolar
% Does OOK and PSK modulation.
%
%   sequencia, bit sequence.
%   amplitude, amplitude of the wave and sinusoide.
%   fs, frequency of the square wave.
%   Tbit, amount of time needed to send one bit.
%   code, 'OOK', or 'PSK'
%   fsBand, frequency of sinusoid to multiply.
%   phase, phase of sinusoid to multiply.
%
function [ x,t ] = BandEmitter( sequencia, amplitude, fs, Tbit, code, fsBand, phase )
    if strcmp(code, 'OOK')
        [x, t] = Emitter(sequencia, amplitude, fs, Tbit, 'U');
    end
    if strcmp(code, 'PSK')
        [x, t] = Emitter(sequencia, amplitude, fs, Tbit, 'B');
    end
    y = amplitude*cos(2*pi*fsBand*t+phase);    
    x = x.*y;
    
%     start=1;
%     for i=1 : length(sequencia)
%         x(start:(start + (fs*Tbit)-1)) = x(start:(start + (fs*Tbit)-1)) .* y(1: fs*Tbit);
%         start = start + fs*Tbit;
%     end
end

