function [ sequencia ] = Receiver( x, Tbit, amplitude, fs, Eb )
    
    %cada 1 ou 0, possui fs pontos no vector.
    increment = fs*Tbit;
    
    %tamanho da trama
    seqSize = round(length(x)/increment);
    
    current = increment/2;
    
    sequencia = zeros( 1, seqSize);
    for i = 1 : seqSize
       if ((x(current)-(amplitude/2))>Eb)
            sequencia(i) = 1;
       end
       current = current + increment;
    end
end

