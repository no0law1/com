% Explica��o
%   Signal to Noise Ratio, quanto maior, melhor a percentagem de erro de bit.
%	Atenua��o, valores negativos invertem o sinal original,
% logo � pior -0.2 que 0.8.
%
%
bits = file2bit('a.txt');
allBits = [bits, bits, bits, bits];

% Bits all corect
[x, t] = BandEmitter(allBits, 1, 100, 1, 'OOK', 10, 0);
plot(t,x);
y = Canal(x, 0.8, 50);
result = BandReceiver(y, t, 1, 1, 100, 10, 0, 'OOK');
disp(' Attenuation 0.8, SNR 50');
disp(isequal(result, allBits));
disp(' Bit Error Rate');
disp(BER_Calculator(allBits, result));

% Not all bits correct due to attenuation
[x, t] = BandEmitter(allBits, 1, 100, 1, 'OOK', 10, 0);
plot(t,x);
y = Canal(x, -0.2, 50);
result = BandReceiver(y, t, 1, 1, 100, 10, 0, 'OOK');
disp(' Attenuation -0.2, SNR 100');
disp(isequal(allBits, result));
disp(' Bit Error Rate');
disp(BER_Calculator(allBits, result));

% Not all bits correct due to snr
[x, t] = BandEmitter(allBits, 1, 100, 1, 'OOK', 10, 0);
plot(t,x);
y = Canal(x, 0.8, 1);
result = BandReceiver(y, t, 1, 1, 100, 10, 0, 'OOK');
disp(' Attenuation 0.8, SNR 1');
disp(isequal(allBits, result));
disp(' Bit Error Rate');
disp(BER_Calculator(allBits, result));