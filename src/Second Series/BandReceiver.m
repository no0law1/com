%
% Function Works only with OOK and PSK modulation.
%
function [ sequencia ] = BandReceiver( x, t, Tbit, amplitude, fs, fsBand, phase, code )
    
    increment = fs*Tbit;
    seqSize = round(length(x)/increment);
    sequencia = zeros(1 , seqSize);
    Eb = 0;
    if strcmp(code, 'OOK')
        Eb = amplitude^2/4 * Tbit;
    end
    y = amplitude*cos(2*pi*fsBand*t+phase);
    
    start = 1;
    for i=1 : seqSize
        E = x(start:start+increment-1)* y(1:increment)';
        start = start + increment;
        if E >= Eb
            sequencia(i) = 1;
        else
            sequencia(i) = 0;
        end
    end
end