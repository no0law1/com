%
% To use this function, you need to put the value of the three bits in
% integer.
%
%
function [ mapObj ] = Create8PSKConstellation( )
    mapObj = containers.Map('KeyType','int32','ValueType','double');
    
    mapObj(0) = 0;
    mapObj(1) = pi/4;
    mapObj(2) = pi/2;
    mapObj(3) = 3*pi/4;
    mapObj(4) = pi;
    mapObj(5) = 5*pi/4;
    mapObj(6) = 3*pi/4;
    mapObj(7) = 7*pi/4;
end

