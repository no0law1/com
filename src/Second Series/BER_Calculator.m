function [ ber ] = BER_Calculator( initialSequence, finalSequence )
    be = 0;
    for i=1 : length(finalSequence)
        if initialSequence(i) ~= finalSequence(i)
            be = be + 1;
        end
    end
    
    ber = be/length(finalSequence);
end

