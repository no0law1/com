%
% Codes a bit sequence to a signal depending of the code given.
%
%   sequencia, bit sequence.
%   amplitude, amplitude of the wave and sinusoide.
%   fs, frequency of the square wave.
%   Tbit, amount of time needed to send one bit.
%   code, 'U', or 'B'
%
function [ x, t ] = Emitter( sequencia, amplitude, fs, Tbit, code)
    t = 0 : 1/fs : (length(sequencia) * Tbit);
    inc = fs*Tbit;
    curr = 1;
    
    zero = 0;
    if code == 'B'
        zero = amplitude*-1;
    end
    
    for i = 1 : length(sequencia)
        if (sequencia(i) == 0)
            x(curr : (curr + inc)) = zero; %De current a current+increment, 0
        else
            x(curr : (curr + inc)) = amplitude; %De current a current+increment, amp
        end

        curr = curr + inc;
    end
end

