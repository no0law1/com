function [ encodedSequence ] = RepetitionEncoding( bitSequence )
    % Repetition code (3,1)
    encodedSequence = zeros(1, length(bitSequence)*3);
    encodedIndex = 1;
    for i=1 : length(bitSequence)
        encodedSequence(encodedIndex : encodedIndex+2) = bitSequence(i);
        encodedIndex = encodedIndex + 3;
    end
end

