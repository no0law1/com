function [ y ] = Canal( x, attenuation, snr)
    y = attenuation*x;
    y = awgn(y,snr,'measured');
end

