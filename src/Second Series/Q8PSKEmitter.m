function [ x, t ] = Q8PSKEmitter( sequence )
    
    map = Create8PSKConstellation();

    Tbit = 1;
    fs = 100;
    t = 0 : 1/fs : (length(sequence) * Tbit);
    x = zeros(1, length(t));
    amplitude = 2;
    frequency = 10;
    
    i = 1;
    while i<length(sequence)
        aux.xi = x;
        
        phase = map(bi2de(seq(i:i+2)));
        aux2.xi = BandEmitter(sequence, amplitude, fs, Tbit, 'PSK', frequency, phase);
        x = signal_sum(t, aux, aux2);
        
        i = i+3;
    end

end

