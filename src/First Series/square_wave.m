function [ t, x ] = square_wave( a, duty_cycle, fs, num_harmonicas )
    Fs = fs*100;
    T = 1/fs;
    t = 0 : 1/Fs : T*5 ;
    
    x = ones(1, length(t)) .* 2*a*duty_cycle;
    
    for i=1 : num_harmonicas
        
        ak = 2*a*duty_cycle*sinc(i*duty_cycle);
        
        if (ak < 0)
            Ok = -pi;
        else
            Ok = 0;
        end
        
        Ak = abs(ak);
        
        s1.xi = x;
        s2.xi = Ak * cos(2*pi*i*fs*t + Ok);
        
        x = signal_sum(t, s1, s2);
        
        %x = x + (Ak * cos(2*pi*i*fs*t + Ok));
    end
    
    plot(t, x);
end

