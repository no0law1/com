function [ mapObj ] = createDTMFMap()
    mapObj = containers.Map('KeyType','char','ValueType','any');

    mapObj('1') = [697, 1209];
    mapObj('2') = [697, 1336];
    mapObj('3') = [697, 1477];
    mapObj('A') = [697, 1633];
    
    mapObj('4') = [770, 1209];
    mapObj('5') = [770, 1336];
    mapObj('6') = [770, 1477];
    mapObj('B') = [770, 1633];

    mapObj('7') = [852, 1209];
    mapObj('8') = [852, 1336];
    mapObj('9') = [852, 1477];
    mapObj('C') = [852, 1633];

    mapObj('*') = [941, 1209];
    mapObj('0') = [941, 1336];
    mapObj('#') = [941, 1477];
    mapObj('D') = [941, 1633];
end

