function [ x, t ] = Emitter( sequencia, amplitude, fs, Tbit )
    % NRZ Unipolar
    t = 0 : 1/fs : (length(sequencia) * Tbit);
    increment = fs*Tbit;
    current = 1;
    
    for i = 1 : length(sequencia)
        if (sequencia(i) == 0)
            x(current : (current + increment)) = 0; %De current a current+increment, 0
        else
            x(current : (current + increment)) = amplitude; %De current a current+increment, amp
        end

        current = current + increment;
    end    
end

