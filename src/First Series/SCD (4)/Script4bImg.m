bitImg = image2bit('136.jpg');
[x, t] = Emitter(bitImg, 2, 8000, 1);
sequence = Receiver(x, 1, 2, 8000);
bitsCorrectlyPassed = isequal(sequence, bitImg)