bitFile = file2bit('a.txt');
[x, t] = Emitter(bitFile, 2, 8000, 1);
sequence = Receiver(x, 1, 2, 8000);
bitsCorrectlyPassed = isequal(sequence, bitFile)