function [ x, t ] = gen_dtmf( key )
    Fs = 8000;
    mapObj = createDTMFMap();
            
    frequencys = mapObj(key);
    
    t = 0 : 1/Fs : 1;
    
    f1.xi = sin(2*pi*frequencys(1)*t);
    f2.xi = sin(2*pi*frequencys(2)*t);
    
    x = signal_sum(t, f1, f2);
    
    plot(t, x);
    p = audioplayer(x,Fs);
    play(p)
    pause(0.5)
    
end

