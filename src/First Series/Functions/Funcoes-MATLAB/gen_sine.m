
function gen_sine()

fo = 1000;
Fs = 8000;
N  = 8;
t  = 0 : 1/Fs : 4; 
y = 0.99*cos( 2*pi*fo*t );          
wavwrite( y, Fs, N, 'sine.wav' );
