
function gen_chirp()

Fs = 8000;
N  = 8;
t  = 0 : 1/Fs : 8; 
y = 0.99*chirp(t,0,1,400);          
wavwrite( y, Fs, N, 'chirp.wav' );
