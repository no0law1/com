%% Script aula pr�tica

%3 - declaracao dos vectores
x=[0 1 4 5 9];
inc=2;
y=[1:inc:20];
t=0:0.01:0.1;

%4 - execute os comandos
x(1)
x(5)
y(x)
y( x(2:3) )
y( x(2:end) )

%5 - obtenha os vectores (operacoes vectorias)
%w=[t(1:6) 0.06:0.02:0.14];
w=[0:0.01:0.05 0.06:0.02:0.14];
g= w + t;
g(7:end) = t(1:5);%t(7:11);
h= w(1:5) + g; %somar vectores com dimensoes erradas
h = w(1:5) + g(2:2:10) %dimensoes certas
%anterior 7 logo aqui
size(g);
length(g)
size(w(1:5));
length(w(1:5))

%6 - opera�oes ponto a ponto e Energia
z=t.^2;
h= w(1:5) .* g(2:2:10)
Eh= sum( h.* h); Eh= h * h';

%8 - produto interno

%9 - Energia de cos
f = 2 * cos( 2*pi* 3* w(1,:));
Ef =  f * f';
l = 10 * f / sqrt(Ef);
Rl = (l*l') / (f*f');

%10 - visualizacao de vectores
x=1 + 3* cos(2*pi*10*t);
plot(x);
figure; plot(t,x);
figure; stem(t,x);

t=-0.1:0.001:0.1;
x=1 + 3* cos(2*pi*20*t);
figure; subplot(1,2,1); plot(t,x); subplot(1,2,2); stem(t,x);

%11 uso de matrizes e vectore

%7 - Matrizes 
A= [1 2 3; 0 1 2; 3 4 5];
y=[4 5 6]
B=[A; y]
size(B);
length(B)
C=B*B'
D=B'*B
M=A; M(4,:)=y; M(:,3)=[]