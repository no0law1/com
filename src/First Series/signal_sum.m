%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunica��es.
%
%
% signal_sum.m
% Recebe:
%       time, escala de tempo onde as v�rias fun��es foram definidas.
%       varargin, array de c�lulas onde se encontra as v�rias fun��es.
% Retorna:
%       sum, soma de todos os sinais recebidos
function [ sum ] = signal_sum( time, varargin )
    numberOfVarArgs = length(varargin);
    if(numberOfVarArgs<1)
        fprintf('Sinais inexistentes');
        return;
    end
    sum = zeros(1, length(time));
    for i=1 : numberOfVarArgs
        fields = fieldnames(varargin{i});
        if length(fields) < 2
            if fields{1} == 'a'
                sum = sum + varargin{i}.a;
            else
                sum = sum + varargin{i}.xi;
            end
        else
            sum = sum + (varargin{i}.a * varargin{i}.xi);
        end
    end
end

