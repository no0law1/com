
function [ b_signal, t ] = make_cos( dc, a, fq, fqq, fs, M )
    ts = 1/fs;
    time = M*ts;
    t = 0 : ts : time;
    b_signal = dc + a.*cos(2*pi*t);
    
end

