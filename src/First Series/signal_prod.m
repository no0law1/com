%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunica��es.
%
%
% signal_prod.m
% Recebe:
%       time, escala de tempo onde as v�rias fun��es foram definidas.
%       varargin, array de c�lulas onde se encontra as v�rias fun��es.
% Retorna:
%       prod, produto de todos os sinais recebidos
function [ prod ] = signal_prod( time, varargin )
    numberOfVarArgs = length(varargin);
    if(numberOfVarArgs<1)
        fprintf('Sinais inexistentes');
        return;
    end
    prod = ones(1, length(time));

    for i=1 : numberOfVarArgs
        fields = fieldnames(varargin{i});
        if length(fields) < 2
            if fields{1} == 'a'
                prod = prod .* varargin{i}.a;
            else
                prod = prod * varargin{i}.xi;
            end
        else
            prod = prod .* (varargin{i}.a * varargin{i}.xi);
        end
    end
end

