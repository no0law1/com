%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunica��es.
%
%
% function_a.m
% Recebe:
%       p, se 1, ent�o guarda como png, sen�o guarda como jpg.
% Retorna:
%       x, array de 10 posi��es.
%       y, 
%       z, Multiplica��o de dois sinais(arrays).
%       w, Valor da multiplica��o entre duas matrizes.
%       v, Array ao contr�rio(z).
function [x,y,z,w,v] = function_a( p )
    clc;    %Limpa a consola.
    x = [1, 2, 3:2:12, -1:-1:-3];   %Cria um array de 10 posi��es.
    y = 4*ones(1,length(x)) + 0.1 * rand(1,length(x));
    y = transpose(y);   %Transposta
    y = y';     %Y � igual � sua transporta.
    z = x .* y; %Multiplica��o ponto a ponto entre x e y.
    w = x * y'; %Multiplica��o de duas matrizes.
    v = fliplr(z);  %Inverte o array.
    close all;  %Fecha todas as figuras.
    figure;     %Apresenta a figura.
    subplot(121);   %Inicia a janela com a figura com dois gr�ficos.First plot will be..
    stem(x);    %Discrete signal x shown in subplot 1.
    title('x - stem');  %Titulo do subplot 1.
    grid on;    %Permite a grelha no subplot 1.
    
    subplot(122);   %Second plot will be..
    plot(y);    %Sinal continuo mostrado no subplot 2.
    title('y - plot');  %Titulo do subplot 2.
    grid on;    %Permite a grelha no subplot 2.
    
    fprintf('w = %d\n', w); %Mostra o resulta da multiplica��o da matriz x e a transposta da matriz y.
    if nargin>0     %Verifica se houve argumentos passados.
        if p==1
            print(gcf,'-dpng','fig1.png');  %Grava o gr�fico como png.
        else
            print(gcf,'-djpeg','fig1.jpg'); %Grava o gr�fico como jpg.
        end     %Termina o �ltimo if.
    end     %Termina o primeiro if.
end     %Termina a fun��o.