%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunica��es.
%
%
% sinusoidal_exponential.m
% Fun��o para desenho de uma fun��o sinusoidal.
% Recebe:
%       A, par�metro de multiplica��o
%       f0, par�metro de frequ�ncia.
%       B, 
% Retorna:
%       t, eixo dos tempos sobre o qual o sinal foi definido.
%       x, amostras do meu sinal.
function [t, x] = sinusoidal_exponential(A, f0, B)

    fprintf(' Fun��o sinusoidal_exponential chamada com %d par�metros de entrada\n', nargin );
    if 0==nargin
        A = 10;
        f0 = 4000;
        B = 800;
    end

    T = 1/f0;

    t = 0 : 1/f0^2 : T*2 ;

    x = A*cos(2*pi*f0*t) .* exp((-B)*t);

    figure;
    plot(t, x);
    title( sprintf('A*cos(2*pi*f0*t) .* exp(-B*t)') );

end