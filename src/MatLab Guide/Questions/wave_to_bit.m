%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunica��es.
%
%
% wave_to_bit.m
% Fun��o que obt�m o sinal �udio presente num ficheiro formato .wav
%   e retorna um vetor com os bits
%   que constituem todas as amostras do sinal �udio.
% Recebe:
%       filename, caminho do ficheiro .wav
% Retorna:
%       wavbinary, vetor com os bits que constituem todas as amostras
%           do sinal �udio.
function [wavbinary] = wave_to_bit( filename )


    [wavdata, Fs] = audioread(filename);

    wavbinary = dec2bin( typecast( single(wavdata(:)), 'uint8')) - '0';

    fprintf('\tDuration: %.3f seconds.\n', length(wavdata) / Fs);

end