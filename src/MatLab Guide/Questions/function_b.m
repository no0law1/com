%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunica��es.
%
%
% function_b.m
% Recebe:
%       p, se p=0, x � um array vazio, sen�o x � um array com uma coluna e v�rias linhas.
% Retorna:
%       x, se p=0, array vazio, sen�o � array com uma coluna e v�rias linhas.
%       y, Resultado da multiplica��o de duas matrizes similares.
%       z, Multiplica��o ponto a ponto de duas matrizes similares.
%       w, array of the sum of rows plus the maximum value of array of max(A).
%       v, Array com uma coluna e v�rias linhas, sinalizando onde existe A>=1.
%       A, Matriz 5 por 5.
function [x,y,z,w,v,A] = function_b( p )
    A = magic(5) / 10;  %Divide todos os valores da matriz por 10.
    B = 2 + eye(5);     %Matriz identidade e a soma de todos os valores da matriz com 2.
    if p ~= 0   %P diferente de 0.
        x = A * B(:,1); %A multiplicado pela primeira coluna de B.
    else
        x = [ ];    %Array vazio.
    end
    y = A * B;  %Multiplica��o de duas matrizes.
    z = A .* B; %Multiplica��o ponto a ponto entre x e y.
    w = sum(A) + max(max(A));   %array of the sum of rows plus the maximum value of array of max(A).
    v = find( A >= 1);  %Descobre os indices em que A>=1.
    A (v) = -1; %Todos os indices encontrados antes ser�o afectados com -1.
    A(:,1) = [ ];   %Primeira coluna vai ser apagada.
end