%
% ISEL - Instituto Superior de Engenharia de Lisboa.
%
% LEIC - Licenciatura em Engenharia Informatica e de Computadores.
%
% Com  - Comunica��es.
%
%
% periodic_signals.m
% Fun��o para desenho de tr�s fun��es no mesmo gr�fico.
% Recebe:
%       A, Constante a somar ao sinal.
%       B, Constante que amplifica o nosso sinal.
%       f0, par�metro de frequ�ncia.
%       O,
% Retorna:
%       t, eixo dos tempos sobre o qual as sincs foram definidas.
%       x, amostras de sinc(t).
%       y, amostras de sinc(at).
%       z, amostras de 
function [t, x, y, z] = periodic_signals(A, B, f0, O)

    fprintf(' Fun��o periodic_signals chamada com %d par�metros de entrada\n', nargin );
    if 0==nargin
        A = -5;
        B = 2;
        f0 = 1000;
        O = pi/4;
    end

    T = 1/f0;

    t = -T : 1/f0^2 : T;

    x = A + B * cos(2*pi*f0*t+O);

    y = (1 + x).^2;

    z = 3 + abs(x);


    figure;
    plot(t, x);
    title( sprintf('%d+%d*cos(2*pi*%d*t+%d)', A, B, f0, O) );

    figure;
    plot(t, y);
    title( sprintf('(1+x(t))^2') );

    figure;
    plot(t, z);
    title( sprintf('3 + abs(x(t))') );

end